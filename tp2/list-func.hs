
-- mymap1 
mymap1 :: (a->b) -> [a] -> [b]
mymap _ [] = []
mymap1 f (h:t) = f h : mymap1 f t    

-- mymap2

-- myfilter1 
myfilter1 :: (a->Bool) -> [a] -> [a] 
myfilter1 _ [] = []
myfilter1 p (h:t) = if p h then h:myfilter1 p t 
                           else myfilter1 p t 

-- myfilter2 

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ mymap1 (*2) [1,2]

