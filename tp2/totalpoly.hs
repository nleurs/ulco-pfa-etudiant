
-- safeTailString 
safeTailString :: String -> String
safeTailString "" = ""
safeTailString (_:t) = t

-- safeHeadString 
safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (h:_) = Just h

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:t) = t

-- safeHead 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (h:_) = Just h

main :: IO ()
main = do
    print $ safeTailString "test"
    print $ safeHeadString "test"
    print $ safeTail [1..10]
    print $ safeHead [1..10]

