import Database.SQLite.Simple (open, close)

import Forum

main :: IO ()
main = do
    conn <- open "forum.db"

    putStrLn "\nForum.dbSelectAll"
    dbSelectAll conn >>= mapM_ print

    putStrLn "\nForum.dbSelectAllSujet"
    dbSelectAllSujet conn >>= mapM_ print
    
    putStrLn "\nForum.dbSelectMessageFromSujetId 1"
    dbSelectMessageFromSujetId conn id >>= mapM_ print

    putStrLn "\nForum.dbInsertMessage"
    dbInsertMessage conn id author text >>= mapM_ print

    close conn

