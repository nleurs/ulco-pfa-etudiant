{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class
import Web.Scotty
import Database.SQLite.Simple
import Data.Text
import Lucid
import TextShow

import Forum

main :: IO ()
main = scotty 3000 $ do
  get "/" $ do
    html $ "<h1> ulcoforum </h1> <a href='/alldata'>alldata</a> - <a href='/allthreads'>allthreads</a> <p> This is ulcoforum </p>"
  get "/alldata" $ do
    res <- 
        liftIO $ withConnection "forum.db" dbSelectAll
    html $ renderText $ ul_ $ mapM_ fmtData res 
  get "/allthreads" $ do
    res <- 
        liftIO $ withConnection "forum.db" dbSelectAllSujet
    html $ renderText $ ul_ $ mapM_ fmtThread res   

  get "/onethread/:id" $ do
        id<- param "id"
        res <-liftIO $ withConnection "forum.db" (dbSelectMessageFromSujetId id)
        html $ renderText $ ul_ $ mapM_ fmtMes res


fmtThread :: (Int,Text) -> Html ()
fmtThread (id, title) = 
   li_ $ do
        a_ [href_ ("/onethread/"<>showt id)] $ toHtml title

fmtData :: (Text, Text, Text) -> Html ()
fmtData (title, author, text) = 
  li_ $ do
    b_ $ toHtml title
    p_ $ do
      toHtml $ author
      ":  "
      toHtml $ text

fmtMes :: (Text, Text) -> Html ()
fmtMes (title, author) = 
  li_ $ do
    p_ $ do
      toHtml $ author
      ":  "
      toHtml $ title


   

     
