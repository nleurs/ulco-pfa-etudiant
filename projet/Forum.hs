{-# LANGUAGE OverloadedStrings #-}

module Forum where

import Data.Text (Text)
import Database.SQLite.Simple

dbSelectAll :: Connection -> IO [(Text, Text, Text)]
dbSelectAll conn = query_ conn "SELECT sujet_title, message_author, message_text \
                                \FROM message \
                                \INNER JOIN sujet ON message_sujet = sujet_id" 

dbSelectAllSujet :: Connection -> IO [(Int, Text)]
dbSelectAllSujet conn = query_ conn "SELECT * FROM sujet"

dbSelectMessageFromSujetId :: Int -> Connection -> IO [(Text, Text)]
dbSelectMessageFromSujetId id conn = query conn "SELECT message_author,message_text FROM message \
                                                \WHERE message_sujet=(?)" (Only id)

--dbInsertMessage :: Int -> Text -> Text -> Connection -> IO []
--dbInsertMessage id author textconn = query conn "INSERT INTO message VALUES((?)(?)(?))"(Only author) (Only text) (Only id)
