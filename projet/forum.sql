-- create database:
-- sqlite3 forum.db < forum.sql

CREATE TABLE sujet (
  sujet_id INTEGER PRIMARY KEY AUTOINCREMENT,
  sujet_title TEXT
);

CREATE TABLE message (
 message_id INTEGER PRIMARY KEY AUTOINCREMENT,
 message_text TEXT,
 message_author TEXT,
 message_sujet INTEGER,
 FOREIGN KEY(message_sujet) REFERENCES sujet(sujet_id)
);

INSERT INTO sujet VALUES(1, 'Vacances 2018-2019');
INSERT INTO sujet VALUES(2, 'Vacances 2019-2020');
INSERT INTO sujet VALUES(3, 'Vacances 2020-2021');

INSERT INTO message VALUES(1, 'rien', 'toto', 1);
INSERT INTO message VALUES(2, 'toujours rien', 'toto', 2);
INSERT INTO message VALUES(3, 'encore rien', 'toto', 2);
INSERT INTO message VALUES(4, 'suspence ... rien', 'toto',3);


