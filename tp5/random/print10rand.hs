
import Control.Monad (forM_)
import System.Random

main :: IO ()
main = do
    forM_ [1..5::Int] $ \i -> do
        x <- randomRIO (1, 100::Int)
        putStrLn $ "iteration " ++ show i ++ ": " ++ show x

