{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class
import Web.Scotty
import Database.SQLite.Simple
import Data.Text
import Lucid
import TextShow

import Movie1


main :: IO ()
main = scotty 3000 $ do
  get "/" $ do
    res <- 
        liftIO $ withConnection "movie.db" dbSelectAllProds
    html $ renderText $ ul_ $ mapM_ fmtProd res

fmtProd :: (Text,Int,Text,Text) -> Html ()
fmtProd (person, year, role, movie) = li_ $ do
    b_ $ toHtml movie
    " ("
    toHtml $ showt year
    "), "
    toHtml $ person
    " ("
    toHtml $ role
    ")"

