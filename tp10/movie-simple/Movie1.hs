{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text (Text)
import Database.SQLite.Simple

dbSelectAllMovies :: Connection -> IO [(Int,Text,Int)]
dbSelectAllMovies conn = query_ conn "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO [(Text,Int,Text,Text)]
dbSelectAllProds conn = query_ conn "SELECT person_name, movie_year, role_name, movie_title \
                                    \FROM prod \ 
                                    \INNER JOIN movie ON prod_movie = movie_id \
                                    \INNER JOIN person ON prod_person = person_id \
                                    \INNER JOIN role ON prod_role = role_id"

dbSelectMoviesFromPersonId ::  Connection -> Int -> IO [[Text]]
dbSelectMoviesFromPersonId  conn p = query conn "SELECT movie_title \
                                                  \FROM movie \
                                                  \INNER JOIN prod \
                                                  \ON prod_movie=movie_id \
                                                  \WHERE prod_person=(?)" (Only p)
-- TODO

