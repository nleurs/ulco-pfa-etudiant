{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Movie where

import Control.Monad.IO.Class (liftIO)
import Database.Selda
import Database.Selda.SQLite

----------------------------------------------------------------------
-- Movie
----------------------------------------------------------------------

data Movie = Movie
  { movie_id :: ID Movie
  , movie_title :: Text
  , movie_year :: Int
  } deriving (Generic, Show)

instance SqlRow Movie

movie_table :: Table Movie
movie_table = table "movie" [#movie_id :- autoPrimary]

----------------------------------------------------------------------
-- Person
----------------------------------------------------------------------

data Person = Person
  { person_id :: ID Person
  , person_name :: Text
  } deriving (Generic, Show)

instance SqlRow Person

person_table :: Table Person
person_table = table "person" [#person_id :- autoPrimary]

----------------------------------------------------------------------
-- Role
----------------------------------------------------------------------

data Role = Role
  { role_id :: ID Role
  , role_name :: Text
  } deriving (Generic, Show)

instance SqlRow Role

role_table :: Table Role
role_table = table "role" [#role_id :- autoPrimary]

----------------------------------------------------------------------
-- Prod
----------------------------------------------------------------------

data Prod = Prod
  { prod_movie :: ID Movie
  , prod_person :: ID Person
  , prod_role :: ID Role
  } deriving (Generic, Show)

instance SqlRow Prod

prod_table :: Table Prod
prod_table = table "prod"
    [ #prod_movie :+ #prod_person :+ #prod_role :- primary
    , #prod_movie :- foreignKey movie_table #movie_id
    , #prod_person :- foreignKey person_table #person_id
    , #prod_role :- foreignKey role_table #role_id
    ]

dbInit :: SeldaT SQLite IO ()
dbInit = do

    createTable movie_table
    tryInsert movie_table
        [ Movie def "Bernie" 1996
        , Movie def "Le Kid" 1921 
        , Movie def "Metropolis" 1927
        , Movie def "Citizen Kane" 1941 ]
        >>= liftIO . print    
        
    createTable person_table
    tryInsert person_table
        [ Person def "Orson Welles"
        , Person def "Charlie Chaplin"
        , Person def "Albert Dupontel"
        , Person def "Claude Perron"
        , Person def "Alfred Abel"
        , Person def "Fritz Lang" ]
        >>= liftIO . print

    createTable role_table
    tryInsert role_table
        [ Role def "Réalisateur"
        , Role def "Acteur"
        , Role def "Producteur" ]
        >>= liftIO . print

    createTable prod_table
    tryInsert prod_table
        [ Prod (toId 1) (toId 3) (toId 1)
        , Prod (toId 1) (toId 3) (toId 2)
        , Prod (toId 1) (toId 4) (toId 2)
        , Prod (toId 2) (toId 2) (toId 1)
        , Prod (toId 2) (toId 2) (toId 2)
        , Prod (toId 2) (toId 2) (toId 3)
        , Prod (toId 3) (toId 5) (toId 2)
        , Prod (toId 3) (toId 6) (toId 1)
        , Prod (toId 4) (toId 1) (toId 1)
        , Prod (toId 4) (toId 1) (toId 2)
        , Prod (toId 4) (toId 1) (toId 3) ]
        >>= liftIO . print



----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------

dbSelectAllMovies :: SeldaT SQLite IO [Movie]
dbSelectAllMovies = query $ select movie_table

dbSelectAllProds :: SeldaT SQLite IO [Movie :*: Person :*: Role]
dbSelectAllProds = query $ do
  prod <- select prod_table
  movie <- select movie_table
  person <- select person_table
  role <- select role_table
  restrict (prod ! #prod_movie .== movie ! #movie_id)
  restrict (prod ! #prod_person .== person ! #person_id)
  restrict (prod ! #prod_role .== role ! #role_id)
  return (movie :*: person :*: role)