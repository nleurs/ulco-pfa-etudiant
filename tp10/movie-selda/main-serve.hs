{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Control.Monad.IO.Class
import Web.Scotty
import Data.Text
import Lucid
import TextShow
import Database.Selda
import Database.Selda.SQLite

import Movie

main :: IO ()
main = scotty 3000 $ do
  get "/" $ do
    res <- liftIO $ withSQLite "movie.db" dbSelectAllProds
    html $ renderText $ ul_ $ mapM_ fmtProd res

fmtProd :: (Movie :*: Person :*: Role) -> Html ()
fmtProd (m :*: p :*: r) = 
    li_ $ do
    b_ $ toHtml $ movie_title m
    " ("
    toHtml $ showt $ movie_year m
    "), "
    toHtml $ person_name p
    " ("
    toHtml $ role_name r
    ")"

