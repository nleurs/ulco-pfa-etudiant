{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

data Artist = Artist {
    artist_id ::ID Artist,
    artist_name :: Text
} deriving (Generic,Show)

instance SqlRow Artist

artist_table :: Table Artist
artist_table = table "artist" [#artist_id :- autoPrimary]

selectArtist :: SeldaT SQLite IO [Artist]
selectArtist = query $ select artist_table

main :: IO ()
main = withSQLite "music.db" selectArtist >>= mapM_ print