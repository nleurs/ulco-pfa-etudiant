{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data MyArtist = MyArtist {
    _id ::Int,
    _name :: Text
} deriving (Generic,Show)

instance FromRow MyArtist where
    fromRow = MyArtist  <$> field <*> field
    
selectAll :: Connection -> IO [MyArtist]
selectAll conn = 
    query_ conn "SELECT * FROM artist"
               
main :: IO ()
main = do
    res <- withConnection "music.db" selectAll 
    mapM_ print res