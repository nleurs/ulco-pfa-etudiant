{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAll :: Connection -> IO [(Text,Text)]
selectAll conn = 
    query_ conn "SELECT artist_name, title_name \
                \FROM title \
                \INNER JOIN artist \
                \on title_artist = artist_id"
                
main :: IO ()
main = 
    withConnection "music.db" selectAll >>= mapM_ print 