{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

data Artist = Artist {
    artist_id ::ID Artist,
    artist_name :: Text
} deriving (Generic,Show)

instance SqlRow Artist


artist_table :: Table Artist
artist_table = table "artist" [#artist_id :- autoPrimary]

data Title = Title {
    title_id ::ID Title,
    title_artist :: ID Artist,
    title_name :: Text
} deriving (Generic,Show)

instance SqlRow Title

title_table :: Table Title
title_table = table "title" [#title_id :- autoPrimary,
                             #title_artist :-foreignKey artist_table #artist_id]

selectAll :: SeldaT SQLite IO [Text :*: Text]
selectAll = query $ do
    a <- select artist_table
    t <- select title_table
    restrict (a ! #artist_id .== t ! #title_artist)
    return (a ! #artist_name :*: t ! #title_name)

main :: IO ()
main = withSQLite "music.db" selectAll >>= mapM_ print
