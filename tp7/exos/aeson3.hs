{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import GHC.Generics
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance FromJSON Person where
        parseJSON = withObject "Person" $ \v -> Person  
            <$> v .: "firstname"
            <*> v .: "lastname"
            <*> (read <$> v .: "birthyear")

main :: IO ()
main = do

    res1 <- eitherDecodeFilesStrict "aeson-test1.json"
    print (res1 :: Either String Person)

    --let res0 = Person "John" "Doe" 1970
    --print res0

