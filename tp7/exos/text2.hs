import qualified Data.Text.IO as TIO
import qualified Data.Text as T

main :: IO ()
main = do
    content <- TIO.readFile "text2.hs"
    putStrLn $ T.unpack content