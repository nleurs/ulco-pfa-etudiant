{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import GHC.Generics
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where
        parseJSON = withObject "Person" $ \v -> Person  
            <$> v .: "firstname"
            <*> v .: "lastname"
            <*> v .: "birthyear"
            <*> v .: "speakenglish"

main :: IO ()
main = do
    let res0 = Person "John" "Doe" "1970" False
    print res0