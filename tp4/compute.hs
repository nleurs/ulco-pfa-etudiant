import Data.List

kernel :: Num a=> a -> a
kernel = (*2) . (+1)

compute :: Num a => [a] -> a
compute = foldl' (\ acc x -> acc + kernel x) 0

main = do
    print $ compute [1..3::Int]
    print $ compute [1..3::Double]

