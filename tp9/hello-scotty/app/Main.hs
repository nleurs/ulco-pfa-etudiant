
{-
import qualified Data.Text.Lazy as L

data Person = Person
    { _name :: L.Text
    , _year :: Int
    }

main :: IO ()
main = putStrLn "TODO"

-}

{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Web.Scotty

import qualified Data.Text.Lazy as L
import Data.Aeson hiding (json)
import GHC.Generics

data Person = Person { _name::L.Text, _age::Int } 

instance ToJSON Person

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  middleware $ staticPolicy $ addBase "static"
  get "/" $ text "this is /"
  get "/route1" $ text "this is /route1"
  get "/route1/route2" $ text "/route1/route2"
  get "/mytext" $ text "this is some text" 
  get "/myhtml" $ html "<h1>this is some html</h1>"
  get "/myjson" $ json ("toto"::String, 42::Int)
  get "/myjson2" $ json (Person "toto" 42)