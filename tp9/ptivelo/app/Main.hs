
{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Web.Scotty

indexPage :: Html ()
indexPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "Ptivelo"
        body_ $ do
             h1_ "Ptivelo"
             div_ $ mapM_ mkRider riders

mkRider :: Rider -> Html ()
mkRider rider = div_ $ do
                    h3_ $ toHtml (_name rider)
                    mapM_ mkImg (_imgs rider)

mkImg :: L.Text -> Html ()
mkImg img = img_ [src_ (L.toStrict img), style_ "height:200px; margin: 0 10px"]

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  middleware $ staticPolicy $ addBase "static"
  get "/" $ text "this is /"
  get "/route1" $ text "this is /route1"
  get "/route1/route2" $ text "/route1/route2"
  get "/mytext" $ text "this is some text" 
  get "/myhtml" $ html "<h1>this is some html</h1>"
  get "/myjson" $ json ("toto"::String, 42::Int)
  get "/riders" $ json riders


riders :: [Rider]
riders =
    [ Rider "Andy Buckworth" ["andy-buckworth.jpg"]
    , Rider "Brandon Loupos" ["brandon-loupos-1.jpg", "brandon-loupos-2.jpg"]
    , Rider "Dave Mirra" ["dave-mirra.jpg"]
    , Rider "Harry Main" ["harry-main.jpg"]
    , Rider "Logan Martin" ["logan-martin-1.jpg", "logan-martin-2.png", "logan-martin-3.jpg"]
    , Rider "Mark Webb" ["mark-webb-1.jpg", "mark-webb-2.jpg"]
    , Rider "Matt Hoffman" ["matt-hoffman.jpg"]
    , Rider "Pat Casey" ["pat-casey-1.jpg", "pat-casey-2.jpg"]
    ]


