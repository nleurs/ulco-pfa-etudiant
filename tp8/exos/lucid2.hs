{-# LANGUAGE OverloadedStrings #-}

import Lucid 

main :: IO ()
main = renderToFile "lucid2.html" maPage

maPage :: Html ()
maPage = do
    doctype_
    head_ $ do
        meta_ [charset_ "utf-8"]
    html_ $ body_ $ do
        h1_ "hello"
        img_ [src_ "https://cybersavoir.csdm.qc.ca/bibliotheques/files/2018/11/10_banques_dimages_gratuites_libres_de_droits-300x169.jpg"]
        p_ $ do
            "this is"
            a_ [href_ "toto.png"] "a link"