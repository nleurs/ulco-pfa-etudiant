{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}


import GHC.Generics
import qualified Data.Text.Lazy as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address :: Address
    } deriving (Generic, Show) 

instance ToJS Person 

data Address = Address 
    { number :: Int
    , road :: T.Text
    , zipcode :: Int
    , city :: T.text
     } deriving (Show)
   

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO ()
main = encodeFile "out-aeson3.json" persons