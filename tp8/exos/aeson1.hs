{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}


import GHC.Generics
import qualified Data.Text.Lazy as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Generic, Show) 

instance ToJSON Person where
    toEncoding (Person f l b) =
        object [
            "first".= f
            , "last" .= l
            , "birth" .= b
        ]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO ()
main = encodeFile "out-aeson1.json" persons