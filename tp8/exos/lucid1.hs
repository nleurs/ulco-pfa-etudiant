{-# LANGUAGE OverloadedStrings#-}

import Lucid
import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T

monHtml :: T.Text -> Html()
monHtml txt = do
    h1_ "hello"
    p_ "world"
    div_ $ p_ "toto"
    div_ $ do
        p_ "tata"
        p_ $ toHtml txt

monH2 :: Html()
monH2 = h2_ "tete"

main :: IO ()
main = T.putStrLn $ renderText $ monHtml "tutu"