
fizzbuzz1 :: [Int] -> [String]--non terminal
fizzbuzz1 [] = []
fizzbuzz1 (h:t) = str : fizzbuzz1 t  
    where str3 = if h `mod` 3 == 0 then "fizz" else ""
          str5 = if h `mod` 5 == 0 then "buzz" else ""
          str35 = str3 ++ str5
          str = if str35 /= "" then str35 else show h


fizzbuzz2 :: [Int] -> [String] -> [String]--terminal
fizzbuzz2 [] acc = acc
fizzbuzz2 (h:t) acc = fizzbuzz2 t (acc ++  [str])
    where str3 = if h `mod` 3 == 0 then "fizz" else ""
          str5 = if h `mod` 5 == 0 then "buzz" else ""
          str35 = str3 ++ str5
          str = if str35 /= "" then str35 else show h

fizzbuzz3 :: [Int] -> [String]--terminal     
fizzbuzz3 l = aux l []
    where aux [] acc = acc
          aux (h:t) acc = aux t (acc ++  [str])
             where str3 = if h `mod` 3 == 0 then "fizz" else ""
                   str5 = if h `mod` 5 == 0 then "buzz" else ""
                   str35 = str3 ++ str5
                   str = if str35 /= "" then str35 else show h   

fizzbuzz :: [String]
fizzbuzz = [show x | x<-[1..15],x `mod` 3 ==0]
            
main :: IO ()
main = do  
    print $ fizzbuzz1 [1..15]
    print $ fizzbuzz2 [1..15] []
    print $ fizzbuzz3 [1..15]
    print $ fizzbuzz  

