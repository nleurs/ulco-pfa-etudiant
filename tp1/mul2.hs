mul2 :: Int -> Int
mul2 x = x * 2

main :: IO ()
main = do
    print $ mul2 42
    print $ mul2 (-42)


