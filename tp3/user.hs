
-- TODO User
data User = User { nom :: String, prenom :: String, age :: Int }
    deriving Show

showUserCheat :: User -> String
showUserCheat u = show u

showUser :: User -> String
showUser (User nom prenom age) = nom ++ " " ++ prenom ++ " " ++ show age ++" ans"

incAge :: User -> User
incAge (User nom prenom age) = User nom prenom (age+1)
--incAge user = user {age = age user + 1}

main :: IO ()
main = do 
    print $ showUserCheat (User "Nicolas" "Leurs" 22)
    print $ showUser (User "Nicolas" "Leurs" 22)
    print $ incAge (User "Nicolas" "Leurs" 22)

