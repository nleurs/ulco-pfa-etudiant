import Data.List (foldl')

-- TODO Abr
data Abr = Empty | Node Int Abr Abr
    deriving (Show)

-- insererAbr 
insererAbr :: Int -> Abr -> Abr
insererAbr x Empty = Node x Empty Empty
insererAbr x (Node y l r)= if x < y 
    then Node y (insererAbr x l) r 
    else Node y l (insererAbr x r)

-- listToAbr 

listToAbr :: [Int] -> Abr
listToAbr [] = Empty
listToAbr (h:t) = insererAbr h (listToAbr t)

-- abrToList 
abrToList ::  Abr -> [Int]
abrToList Empty = []
abrToList (Node x l r) = abrToList l ++ [x] ++ abrToList r


main :: IO ()
main = do
    print $ listToAbr [42,13]
    print $ listToAbr [12,54,65,2]
    print $ abrToList (Node 2 Empty (Node 65 (Node 54 (Node 12 Empty Empty) Empty) Empty))

