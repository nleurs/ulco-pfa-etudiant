
-- TODO List
data List a = Empty | Cons a (List a)
    deriving Show

-- sumList 
sumList :: Num a => List a -> a
sumList Empty = 0
sumList (Cons h t) = h + sumList t

-- concatList 
concatList :: List a -> List a -> List a
concatList Empty y = y
concatList (Cons h t) y = Cons h (concatList t y)

-- toHaskell 
toHaskell :: List a -> [a]
toHaskell Empty = []
toHaskell (Cons h t) = h:toHaskell t

-- fromHaskell 
fromHaskell :: [a] -> List a
fromHaskell [] = Empty
fromHaskell (h:t) = Cons h (fromHaskell t)

-- myShowList
myShowList :: Show a => List a -> [Char]
myShowList Empty = ""
myShowList (Cons h t) = show h ++ " " ++ myShowList t

main :: IO ()
main = do
    print $ sumList (Cons 5 (Cons 2 (Cons 3 Empty))) 
    print $ concatList (Cons "bla" (Cons "bla" Empty)) (Cons "bla" (Cons "bla" Empty)) 
    print $ toHaskell (Cons "foo" (Cons "bar" Empty ))
    print $ fromHaskell ["foo","bar"]
    print $ myShowList (Cons "bla" (Cons "bla" Empty))

